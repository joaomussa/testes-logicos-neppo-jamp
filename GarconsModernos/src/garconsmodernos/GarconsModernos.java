package garconsmodernos;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author John
 */
public class GarconsModernos {

    public static void main(String[] args) {
        String saida = "";
        Map<String, Integer> romanMap = new LinkedHashMap<>();
        
        romanMap.put("I",1);
        romanMap.put("V",5);
        romanMap.put("X",10);
        romanMap.put("L",50);
        romanMap.put("C",100);
        romanMap.put("D",500);
        romanMap.put("M",1000);
        
        Scanner s = new Scanner(System.in);
        
        System.out.println("Digite um numero:");
        Integer input = Integer.parseInt(s.nextLine());
        if(input/1000 >= 1){
            saida = saida + writeNum((int)input/1000 * 1000, romanMap);
            input = input - ((int)input/1000 * 1000);
        }
        if(input/100 >= 1){
            saida = saida + writeNum((int)input/100 * 100, romanMap);
            input = input - ((int)input/100 * 100);
        }
        if(input/10 >= 1){
            saida = saida + writeNum((int)input/10 * 10, romanMap);
            input = input - ((int)input/10 * 10);
        }
        if(input >= 1){
            saida = saida + writeNum((int)input, romanMap);
            input = input - ((int)input);
        }
        
        System.out.println(saida);
    }
    
    private static String getRomanKey(Integer value, Map<String, Integer> romanMap) throws Exception {
        
        for(Map.Entry<String, Integer> romanKey : romanMap.entrySet()) {
            if(value == romanKey.getValue())
                return romanKey.getKey();
        }
        throw new Exception();
    }
    
    private static Integer getLowerNumPart(Integer input, Map<String, Integer> romanMap) {
        Integer actBig = 1;
        for(Map.Entry<String, Integer> romanKey : romanMap.entrySet()) {
            if(input >= romanKey.getValue())
                actBig = romanKey.getValue();
            else
                break;
        }
        return actBig;
    }

    private static Integer getNextBiggestNumPart(int value) {
        Integer[] romanNumbers = {1,5,10,50,100,500,1000};
        for(Integer actNum: romanNumbers){
            if(actNum > value)
                return actNum;
        }
        return -1;
    }
    
    private static String writeNum(Integer value, Map<String, Integer> romanMap) {
        String saida = "";
        Integer lowerNumPart = getLowerNumPart(value,romanMap);
        Integer NextBiggestNumPart = getNextBiggestNumPart(value);
        
        try{
            while(value > 0){
                
                if(value == 4){
                    saida = saida + typeRomanXTimes(getRomanKey(getLowerNumPart(getLowerNumPart(value,romanMap)-1,romanMap), romanMap),1); 
                    saida = saida + typeRomanXTimes(getRomanKey(getLowerNumPart(NextBiggestNumPart,romanMap), romanMap),1); 
                    value = value - (NextBiggestNumPart - getLowerNumPart(getLowerNumPart(value,romanMap)-1,romanMap));
                    lowerNumPart = getLowerNumPart(value,romanMap);
                    NextBiggestNumPart = getNextBiggestNumPart(value);
                }
                if(value == 40 || value == 400){
                    saida = saida + typeRomanXTimes(getRomanKey(getLowerNumPart(value,romanMap), romanMap),1); 
                    saida = saida + typeRomanXTimes(getRomanKey(getLowerNumPart(NextBiggestNumPart,romanMap), romanMap),1); 
                    value = value - (NextBiggestNumPart - getLowerNumPart(getLowerNumPart(value,romanMap)-1,romanMap));
                    lowerNumPart = getLowerNumPart(value,romanMap);
                    NextBiggestNumPart = getNextBiggestNumPart(value);
                }
                if(NextBiggestNumPart < 0 && value > 0){
                    //Numero maior que 1000
                   saida = saida + typeRomanXTimes(getRomanKey(lowerNumPart, romanMap),(int) value/lowerNumPart); 
                   value = value - lowerNumPart * ((int) value/lowerNumPart);
                   lowerNumPart = getLowerNumPart(value,romanMap);
                   NextBiggestNumPart = getNextBiggestNumPart(value);
                }else if(value > 0 && NextBiggestNumPart > 0){
                    //297
                    if(((double) value/(double) NextBiggestNumPart) >= 0.9){
                        //usa maior
                        saida = saida + typeRomanXTimes(getRomanKey(getLowerNumPart(getLowerNumPart(value,romanMap)-1,romanMap), romanMap),1); 
                        saida = saida + typeRomanXTimes(getRomanKey(getLowerNumPart(NextBiggestNumPart,romanMap), romanMap),1); 
                        value = value - (NextBiggestNumPart - getLowerNumPart(getLowerNumPart(value,romanMap)-1,romanMap));
                        lowerNumPart = getLowerNumPart(value,romanMap);
                        NextBiggestNumPart = getNextBiggestNumPart(value);
                    }else{
                        //usa menor
                        saida = saida + typeRomanXTimes(getRomanKey(lowerNumPart, romanMap),(int) value/lowerNumPart); 
                        value = value - lowerNumPart * ((int) value/lowerNumPart);
                        lowerNumPart = getLowerNumPart(value,romanMap);
                        NextBiggestNumPart = getNextBiggestNumPart(value);
                    }
                }
            }
        }catch(Exception ex){
            //Key nao encontrada
        }
        return saida;
    }

    private static String typeRomanXTimes(String x, int i) {
        String saida = "";
        while(i > 0){
            saida = saida + x;
            i--;
        }
        return saida;
    }
}
