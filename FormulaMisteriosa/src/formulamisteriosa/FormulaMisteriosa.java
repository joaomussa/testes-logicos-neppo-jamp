package formulamisteriosa;
import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author John
 */
public class FormulaMisteriosa {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        System.out.println("Digite um numero:");
        
        //Como foi desctrito para se usar um unsigned long com objetivo de se ter 64 bits para valores (sem bit de sinal), 
        //foi usado BigInteger, pois a saída poderá ser maior que a entrada
        BigInteger input = new BigInteger(s.nextLine());
        
        System.out.println("Formula mágica: "+ FormulaM(input));
    }
    
    //Método responsável por retornar a Quantidade de numeros sequênciais + O próprio número repetindo o processo até o final do numero.
    public static BigInteger FormulaM(BigInteger longInput){
        String input = String.valueOf(longInput);
        String saida = "";
        char actNum;
        int cont = 0, qntActNum;
        
        while(cont < input.length()){
            actNum = input.toCharArray()[cont];
            qntActNum = getContSeqNum(input,cont);
            saida = saida + qntActNum + actNum;
            cont = cont + qntActNum;
        }
        
     return new BigInteger(saida);   
    }
    
    //Método responsável por pegar a quantidade de números iguais na sequência da string, a partir de determinada posição
    private static int getContSeqNum(String input, int pos) {
        char ActNum = input.toCharArray()[pos];
        int amount = 1;
        
        while((pos+1) < input.length() && input.toCharArray()[pos+1] == ActNum){
            pos++;
            amount++;
        }
        
        return amount;
    }
}
