package maquinadotempo;
import java.util.Scanner;

/**
 *
 * @author John
 */
public class MaquinaDoTempo {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        System.out.println("Digite uma data:");
        String input = s.nextLine();
        
        String[] date = input.split(" ");
        
        System.out.println("Saida: "+ ValiDate(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2])));
    }

    //Método responsável por validar datas
    private static boolean ValiDate(int day, int month, int year) {
        //Array para comparar a quantidade de dias por mês básica (desconsiderando anos bissextos)
        int[] monthsDays = {31,28,31,30,31,30,31,31,30,31,30,31};
        
        //Variável de incremento para dia limite de fevereiro em anos bissextos
        int extraDayToAdd = isAnoBissexto(year);

        return month <= 12 
                &&  1 <= day
                    && day <= (monthsDays[month - 1] + extraDayToAdd);
    }

    //Método responsável por retornar 1 se o ano é bissexto ou 0 se o ano não for
    private static int isAnoBissexto(int year) {
        if(year%4 == 0){
            if(year%400 == 0)
                return 1;
            else if(year%100 != 0)
                return 1;
        }
        return 0;
    }
}
